    #путь до локальной версии исходников senseara
SANSEARA_LOC=r'C:\Users\balakina-eu\Documents\Outside\senseara\src'
SANSEARA_DISK=r'X:\FieldCalibration\! Sensara Recalibration\senseara\src' 


REF_NO2_RAW = r'..\data\raw_reference\Optec_NO2.csv'
REF_O3_RAW = r'..\data\raw_reference\Optec_O3.csv'

RH_STABLE_RAW = r'..\data\raw_sensors\G1000552.csv'
RH_STREET_RAW = r'..\data\raw_sensors\G10006F3.csv'


REF_NO2 =  r'..\data\clean_united\Optec_NO2_clean.csv'
REF_O3 =  r'..\data\clean_united\Optec_O3_clean.csv'

RH_STABLE = r'..\data\clean_united\G1000552_clean.csv'
RH_STREET = r'..\data\clean_united\G10006F3_clean.csv'

temp_recalc={
                "CO" : False,
                "NO2": 20,
                "O3" : 20
            }

sens_coeff={
            "NO2":
                    [
                        {
                            "comments": "Cтарая зависимость NO2 с нормировкой на 20С",
                            'ft0': 0.5058,
                            'ft1': 0.015136,
                            'ft2': 0.000479,
                        },
                        {
                            "comments": "Cредняя зависимость NO2 с продувки G3 при характерной влажности с нормировкой на 20С",
                            'ft0': 1.084013,
                            'ft1': -0.003494,
                            'ft2': -0.00003533,
                        }
                    ],
            "CO":
                    [
                        {
                            "comments": "Cтарая зависимость CO с нормировкой на 20С",
                            'ft0': 0.7048,
                            'ft1': 0.014620,
                            'ft2': 0.00000693,
                        },
                        {
                            "comments": "Cредняя зависимость CO с продувки G3 при характерной влажности с нормировкой на 20С",
                            'ft0': 0.565761,
                            'ft1': 0.018516,
                            'ft2': 0.00015980,
                        },
                    ],
            "O3":
                    [
                        {
                            "comments": "Cтарая зависимость O3 с нормировкой на 20С",
                            'ft0': 0.5058,
                            'ft1': 0.015136,
                            'ft2': 0.000479,
                        },
                        {
                            "comments": "Cредняя зависимость O3 с продувки G3 при характерной влажности с нормировкой на 20С",
                            'ft0': 1.084013,
                            'ft1': -0.003494,
                            'ft2': -0.00003533,
                        },
                    ],
            }

gas_bounds={
            "CO" :([-100, -100, -0.001, -200],[100, 100, 0.3, 200]),
            "NO2":([-20, -20, -10, -200],[100, 20, 0.3, 200]),
            "O3" :([-20, -20, -10, -100],[10, 2, 0.3, 100]),
           }

gas_X0={
            "CO": [
                -0.4, 
                -0.04, 
                0.12, 
                0.022
            ],
            "NO2": [
                30,
                -0.07, 
                0.15, 
                -0.05
            ],
            "O3": [
                -3, 
                -0.07, 
                0.1, 
                -0.03
            ],
           }

Q={
            "CO" : 0.001,
            "NO2": 0.27,
            "O3" : 0.45,
    }

sens_calibration_interval={
                            "CO" :["2022.08.01","2024.04.01"],
                            "NO2":["2022.01.17","2023.04.21"],
                            "O3" :["2023.03.20","2024.04.01"],
                          }
