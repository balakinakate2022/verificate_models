# Верификация алгоритмов восстановления концентрации на балконном полигоне

[Статья на Confluence](https://confluence.cityair.dev/pages/viewpage.action?pageId=231833993)

Структура папок:

```
├───configs
│   └───config                  файл с названиями путей
├───data
│   ├───clean_reference         очищенные референсы c оптеков b gctdljht, переведённые в нск время
│   ├───clean_united            очищенные данные с сенсоров для RH-stable и RH-stree, переведённые в нск
│   ├───G3_raw                  данные сенсоров с G3 без обработки с R=47Ом
│   │   ├───G3_NO2                  отдельно для NO2   
│   │   └───G3_O3                   отдельно для O3
│   ├───plots                   графики, получившиеся в процессе всего
│   ├───pseudoref               исторические данные для обучения псевдореференса 
│   ├───raw_reference           референсы c оптеков без обработки
│   └───raw_sensors             данные с сенсоров для RH-stable и RH-street без обработки  
├───notebooks
└───src
    ├───rh_ah_convert.py
    └───utils.py
```