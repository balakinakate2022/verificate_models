import sys
#путь до лакальной версии исходников senseara
sys.path.append(r'X:\FieldCalibration\! Sensara Recalibration\senseara\src')
from IO.request import TrinoRequest
from IO.files import (fetch_station_packets, read_csvs)
from utils.plots import draw_plot
import pandas as pd
import os
import json
from datetime import datetime, timedelta
from tqdm.notebook import tqdm

#локальное время начала загрузки данных
start = "2024.03.15 14:15"

# путь до файла с событиями
events_path=r"events.py"

#пути для сохранения данных
main_path="data"
raw=fr"{main_path}/Raw"
src=fr"{main_path}/Src"
plots=fr"{main_path}/Plots"

#создадим директории под данные, если их нет
os.makedirs(raw, exist_ok=True)
os.makedirs(src, exist_ok=True)
os.makedirs(plots, exist_ok=True)


resample_period=20

#список станций и их модулей
# Case="CA01PM000740"
# G3_stations=["CA01PM000740","CA01PM020753"]
stations=["CA01PM000740"]


rooftop_modules=[
					# "G200036D",
					# "G3000030",
					# "G10006F3",
					# "G10006EF",
					# "G1000884",
					# "G100097F",
					# "G20009C9",
					# "G20009C8",
					# # "G200095A",
					# "G2000899",
					"Optec_NO2",
					"Optec_O3",
					"G1000552",
					"G10006F3",
					"G20009D9",
					"G10005DB",
					"G200036D",
					"G2000780"
				]

rename_cols={
				# ("CA01PM000740", 'T station'): "T, улица (BME280)",
				# ('G1000001', 'NO2t')	 : "T, в коробке около G1 (DS18B20)",
				# ('G1000001', 'COt')	  : "T, вход (DS18B20)",
				# ('G20004D9','SO2t')	 : "T, жидкость в теплообменнике (DS18B20)",
				# ('G20004D9','H2St')	 : "T, выход из теплообменника (DS18B20)",
				# ('G1000001', 'MT')	: "T, верхний модуль, левая линия (HPP845E)",
				# ('G20004D9','MT')	: "T, нижний модуль, правая линия (HPP845E)",
				# ('G1000001', 'O3t')	  : "T, выход (DS18B20)",
				# ('G1000001', 'MH')	: "RH, верхний модуль, левая линия (HPP845E)",
				# ("CA01PM000740", "RH")	   : "RH, улица (BME280)",
				# ('G300001D','NO2op1')	: "NO2op1_77"
			}
#реквест для доступа в Trino. Надо вписать свои login и password. Я беру из переменных окружения.
tr = TrinoRequest(os.environ["TRINO_LOGIN"], os.environ["TRINO_PASSWORD"])



gases=["CO", "SO2", "H2S", "NO2", "O3"]

#определяем локальные функции
def parse_G3_data_from_Trino(d):
	"""Функция парсит и возвращает датафрейм Трино со столбцами данных из G3"""
	d.loc[d["g3"]=="empty",'g3']=None
	if "g3_data" in d.keys():
		g3=[]
		G3_data_tmp=pd.json_normalize(d["g3_data"].fillna("{}").apply(json.loads)).set_index(d.index)
		d=pd.concat([d, G3_data_tmp], axis=1)
		d.drop(["g3_data"], axis=1, inplace=True, errors='ignore')
		for col in d.keys():
			if col[:2]=="G3":
				g3.append(col)
		d.rename(columns={col:col.upper().replace(".", " ").replace("_", "").replace("OP", "op").replace("T", "t") for col in g3}, inplace=True) #грязновато тут с реплейсами...
		d.drop(columns=["g3"], inplace=True)
		return d

def add_source_to_col(d):
	"""Функция добавляет названия столбцов датафрейма Трино названия источника данных"""
	dev=[]
	G1_cols=['NO2op1', 'NO2op2', 'NO2t', 'O3op1', 'O3op2', 'O3t', 'COop1', 'COop2', 'COt', 'g1_mt', 'g1_mh']
	G2_cols=['H2Sop1', 'H2Sop2', 'H2St', 'SO2op1', 'SO2op2', 'SO2t', 'g2_mt', 'g2_mh']
	G1=d["G1"].unique().tolist()
	if "empty" in G1:
		G1.remove("empty")
	for g1 in G1:
		for col in G1_cols:
			d[f"{g1} {col}"]=d.loc[d["G1"]==g1, col]
	d.drop(columns=G1_cols+["G1"], axis=1, errors="ignore", inplace=True)
	G2=d["G2"].unique().tolist()
	if "empty" in G2:
		G2.remove("empty")
	for g2 in G2:
		for col in G2_cols:
			d[f"{g2} {col}"]=d.loc[d["G2"]==g2, col]
	d.drop(columns=G2_cols+["G2"], axis=1, errors="ignore", inplace=True)
	d.rename(columns={col:(col[:col.find(" ")+1]+col[col.find("_")+1:].upper()) for col in d.keys() if ('mh' in col) or ("mt" in col)}, inplace=True)
	st=d['Station Serial'].unique()[0]
	d.rename(columns={col:f"{st} {col}" for col in d.keys() if "G" not in col}, inplace=True)

def Trino_to_multiIndex(d, dropna=False):
	"""Функция возвращает датафрейм из Трино в виде мультииндексного.
	dropna=True - выбросить полностью пустые строки и столбцы"""
	d=parse_G3_data_from_Trino(d)
	add_source_to_col(d)
	new_indexes=[]
	for col in d.columns:
		new_indexes.append(col[:col.find(" ")])
	d.columns = [new_indexes, d.rename(columns={col:col[col.find(" ")+1:] for col in d.columns}).columns]
	if dropna==True:
		d.dropna(how="all", axis=0, inplace=True)
		d.dropna(how="all", axis=1, inplace=True)
	return d


def convert_time(start, shift=0):
	"""
	Конвертирует время из  формата '%Y.%m.%d %H:%M' (2023.05.01 15:53) или '%Y.%m.%d' (2023.05.01) в '%Y-%m-%dT%H:%M:00' для функции fetch_station_packets
	с возможностью сдвига на {shift}.
	"""
	try:
		start_shift=(datetime.strptime(start, '%Y.%m.%d %H:%M')+timedelta(hours=shift)).strftime('%Y-%m-%dT%H:%M:00')
	except:
		start_shift=(datetime.strptime(start, '%Y.%m.%d')+timedelta(hours=shift)).strftime('%Y-%m-%dT%H:%M:00')
	return start_shift

def get_finish_time(finish, shift=0):
	"""
	Конвертирует время из  формата '%Y.%m.%d %H:%M' (2023.05.01 15:53) или '%Y.%m.%d' (2023.05.01) в '%Y-%m-%dT%H:%M:00' для функции fetch_station_packets
	с возможностью сдвига на {shift}. Можно указать "now"
	"""
	if finish=="now":
		finish_shift=(datetime.now()+timedelta(hours=shift)).strftime('%Y-%m-%dT%H:%M:00')
	else:
		finish_shift=convert_time(finish, shift=shift)
	return finish_shift

def read_events(events_path, reverse=True):
	"""
	Функция грузит словарь из файла по пути events_path с событиями для отображения на графике.
	{reverse}=True - запись в файле события сток типа {событие};{время}
	"""
	events = {}
	with open(events_path, "r", encoding="utf-8") as file:
		for line in file:
			if line[0]!="#":
				key, value = line.split(';')
				if reverse==True:
					events[key] = value.replace("\n","")
				else:
					events[value.replace("\n","")] = key
	return events

def crop_events(events, crop_s="", crop_f=""):
    """
    Обрезает словарь с событиями раньше crop_s или позже crop_f при их наличии
    Допустимый формат дат '%Y.%m.%d %H:%M' или '%Y.%m.%d'
    """
    if crop_s=="" and crop_f=="":
        return events
    if crop_s!="":
        try:
            crop_t1=datetime.strptime(crop_s, '%Y.%m.%d %H:%M')
        except:
            try:
                crop_t1=datetime.strptime(crop_s, '%Y.%m.%d')
            except:
                print(f"Неверный формат даты обрезки crop_s \'{crop_s}\'!")
                return events
        
    if crop_f!="":
        try:
            crop_t2=datetime.strptime(crop_f, '%Y.%m.%d %H:%M')
        except:
            try:
                crop_t2=datetime.strptime(crop_f, '%Y.%m.%d')
            except:
                print(f"Неверный формат даты обрезки crop_f \'{crop_f}\'!")
                return events

    drop_keys=[]
    for key in events.keys():
        if type(events[key])!=datetime:
            try:
                events[key]=datetime.strptime(events[key], '%Y.%m.%d %H:%M')
            except:
                try:
                    events[key]=datetime.strptime(events[key], '%Y.%m.%d')
                except:
                    print(f"Неверный формат даты \"{events[key]}\" для события \"{key}\"")
        if crop_s!="":
            if events[key]<crop_t1:
                drop_keys.append(key)
        if crop_f!="":
            if events[key]>crop_t2:
                drop_keys.append(key)
    new_events=events.copy()
    for key in drop_keys:
        new_events.pop(key)
    return new_events

		
def find_files(PATH, name="", ext="csv", excl="", show=False):
	"""
	Функция возвращает список с названиями файлов, содержащими {name} в имени, по пути {PATH} с расширение {ext}.
	exlc - части названия, которая не должна содержаться в файле
	Может написать что она нашла при show=True.
	"""
	raw_data_files=[]
	for i in os.listdir(path=PATH):
		if i.endswith(ext):
			if (name in i) and ((excl not in i) or (excl=="")):
				raw_data_files.append(i)
	if show==True:
		print(f"В папке {PATH} найдено {len(raw_data_files)} файлов с расширением {ext}", *["\t"+file for file in raw_data_files], sep="\n" )
	return raw_data_files