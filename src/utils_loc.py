import os
from datetime import datetime, timedelta
import pandas as pd
import json

from sklearn.metrics import mean_absolute_percentage_error
from sklearn.metrics import r2_score
from sklearn.metrics import root_mean_squared_error
from scipy.stats import pearsonr

import sys
sys.path.insert(0, '..')
from configs.config import *

sys.path.append('X:\FieldCalibration\! Sensara Recalibration\senseara\src')
from processing.data_handling import extract_baseline
from utils.plots import draw_plot

def calc_compare(y_true, y_pred):
	mape = mean_absolute_percentage_error(y_true, y_pred)
	r2 = r2_score(y_true, y_pred)
	rmse = root_mean_squared_error(y_true, y_pred)
	pearson = pearsonr(y_true, y_pred)[0]
	return [pearson, rmse, r2, mape]

def find_files(PATH, name="", ext="csv", excl="", show=False):
    """
    Функция возвращает список с названиями файлов, содержащими {name} в имени, по пути {PATH} с расширение {ext}.
    exlc - части названия, которая не должна содержаться в файле
    Может написать что она нашла при show=True.
    """
    #проверка наличия папки
    if os.path.exists(PATH):
        raw_data_files=[]
        for i in os.listdir(path=PATH):
            if i.endswith(ext):
                if (name in i) and ((excl not in i) or (excl=="")):
                    raw_data_files.append(i)
        if show==True:
            print(f"В папке \'{PATH}\' найдено {len(raw_data_files)} файла/ов с расширением \'{ext}\'", *["\t"+file for file in raw_data_files], sep="\n" )
        return raw_data_files
    else:
        print(f"Запрашиваемая директория '{PATH}' отсутствует на диске!")
        return []
    

def convert_time(start, shift=0):
	"""
	Конвертирует время из  формата '%Y.%m.%d %H:%M' (2023.05.01 15:53) или '%Y.%m.%d' (2023.05.01) в '%Y-%m-%dT%H:%M:00' для функции fetch_station_packets
	с возможностью сдвига на {shift}.
	"""
	try:
		start_shift=(datetime.strptime(start, '%Y.%m.%d %H:%M')+timedelta(hours=shift)).strftime('%Y-%m-%dT%H:%M:00')
	except:
		start_shift=(datetime.strptime(start, '%Y.%m.%d')+timedelta(hours=shift)).strftime('%Y-%m-%dT%H:%M:00')
	return start_shift

def get_finish_time(finish, shift=0):
	"""
	Конвертирует время из  формата '%Y.%m.%d %H:%M' (2023.05.01 15:53) или '%Y.%m.%d' (2023.05.01) в '%Y-%m-%dT%H:%M:00' для функции fetch_station_packets
	с возможностью сдвига на {shift}. Можно указать "now"
	"""
	if finish=="now":
		finish_shift=(datetime.now()+timedelta(hours=shift)).strftime('%Y-%m-%dT%H:%M:00')
	else:
		finish_shift=convert_time(finish, shift=shift)
	return finish_shift

def Trino_to_multiIndex(d, dropna=False):
	"""Функция возвращает датафрейм из Трино в виде мультииндексного.
	dropna=True - выбросить полностью пустые строки и столбцы"""
	d=parse_G3_data_from_Trino(d)
	add_source_to_col(d)
	new_indexes=[]
	for col in d.columns:
		new_indexes.append(col[:col.find(" ")])
	d.columns = [new_indexes, d.rename(columns={col:col[col.find(" ")+1:] for col in d.columns}).columns]
	if dropna==True:
		d.dropna(how="all", axis=0, inplace=True)
		d.dropna(how="all", axis=1, inplace=True)
	return d

def parse_G3_data_from_Trino(d):
	"""Функция парсит и возвращает датафрейм Трино со столбцами данных из G3"""
	d.loc[d["g3"]=="empty",'g3']=None
	if "g3_data" in d.keys():
		g3=[]
		G3_data_tmp=pd.json_normalize(d["g3_data"].fillna("{}").apply(json.loads)).set_index(d.index)
		d=pd.concat([d, G3_data_tmp], axis=1)
		d.drop(["g3_data"], axis=1, inplace=True, errors='ignore')
		for col in d.keys():
			if col[:2]=="G3":
				g3.append(col)
		d.rename(columns={col:col.upper().replace(".", " ").replace("_", "").replace("OP", "op").replace("T", "t") for col in g3}, inplace=True) #грязновато тут с реплейсами...
		d.drop(columns=["g3"], inplace=True)
		return d


def add_source_to_col(d):
	"""Функция добавляет названия столбцов датафрейма Трино названия источника данных"""
	dev=[]
	G1_cols=['NO2op1', 'NO2op2', 'NO2t', 'O3op1', 'O3op2', 'O3t', 'COop1', 'COop2', 'COt', 'g1_mt', 'g1_mh']
	G2_cols=['H2Sop1', 'H2Sop2', 'H2St', 'SO2op1', 'SO2op2', 'SO2t', 'g2_mt', 'g2_mh']
	G1=d["G1"].unique().tolist()
	if "empty" in G1:
		G1.remove("empty")
	for g1 in G1:
		for col in G1_cols:
			d[f"{g1} {col}"]=d.loc[d["G1"]==g1, col]
	d.drop(columns=G1_cols+["G1"], axis=1, errors="ignore", inplace=True)
	G2=d["G2"].unique().tolist()
	if "empty" in G2:
		G2.remove("empty")
	for g2 in G2:
		for col in G2_cols:
			d[f"{g2} {col}"]=d.loc[d["G2"]==g2, col]
	d.drop(columns=G2_cols+["G2"], axis=1, errors="ignore", inplace=True)
	d.rename(columns={col:(col[:col.find(" ")+1]+col[col.find("_")+1:].upper()) for col in d.keys() if ('mh' in col) or ("mt" in col)}, inplace=True)
	st=d['Station Serial'].unique()[0]
	d.rename(columns={col:f"{st} {col}" for col in d.keys() if "G" not in col}, inplace=True)



#фиттируем базовую линию
def fit_baseline(gas, model, df, g_bounds, X0, Q):
    """
    Функция фиттирует базовую линию и рисует её график.
    {gas} - название газа,
    {models} - модель для газа типа NormModelTExpH2S,
    {df} - датафрейм, содержащий {gas}op1-{gas}op2, {gas}t,
    {g_bounds} - граничные условия для базовой линии,
    {X0} - начальные условия для базовой линии,
    {Q} - квантиль фиттирования базовой линии
    """
    model._fit_normalizer(
                            extract_baseline(
                                             df.rename(columns={f"{gas}t":"T"}),
                                             gas,
                                             quantile=Q
                                            ),
                            bounds=g_bounds,
                            p0=X0
                          )
    # смотрим базовые линии
    # display(
    #             draw_plot(
    #                         df.rename(columns={f"{gas}t":"T"}).set_index("T")[f"{gas}op1-{gas}op2"],
    #                         model.normalizer.sample_baseline().rename("Базовая линия"),
    #                         extract_baseline(
    #                                          df.rename(columns={f"{gas}t":"T"}),
    #                                          gas,
    #                                          quantile=Q
    #                                         ).set_index("T")[f"{gas}op1-{gas}op2"].rename("Точки для фиттирования базовой линии"),
    #                         mode='markers',
    #                         title=f"Базовая линия псевдореференса {gas} из {model.serial_number}",
    #                         yaxis_title="dU, мВ",
    #                         xaxis_title="T, C",
    #                     )
    #         )
    print("Параметры модели после калибровки базовой линии:", model, *[f"{a:20s}: {b:10.2e}" for a, b in model.params.items()], sep="\n")
    return model

#фиттируем чувствительность
def fit_sens(gas, model, df, ref, t_s, t_f):
    """
    Функция фиттирует чувствительность по эталону {ref} и рисует её график.
    {gas} - название газа,
    {models} - модель для газа типа NormModelTExpH2S с откалиброванной базовой линией,
    {df} - датафрейм, содержащий {gas}op1-{gas}op2, {gas}t, CO для NO2 и CO/NO2 для O3
    {ref} - датафрейм, содержащий данные с эталона
    Из всего датафрейма для фиттирования вырезается интервал с {t_s} до {t_f}
    """
    module_set={
                    "CO":[f"{gas}t", f"{gas}op1", f"{gas}op2", f"{gas}op1-{gas}op2"],
                    "NO2":[f"{gas}t", f"{gas}op1", f"{gas}op2", f"{gas}op1-{gas}op2", "CO"],
                    "O3":[f"{gas}t", f"{gas}op1", f"{gas}op2", f"{gas}op1-{gas}op2", "CO", "NO2"]
               }
    
    fit_params={
                    "CO":[ "slope", "intercept"],
                    "NO2":[ "slope", "intercept"],
                    "O3":[ "slope", "intercept", "NO2"],
               }
    
    gas_P0=    {
                    "CO" :[1, 1],
                    "NO2":[1, 1],
                    "O3" :[1, 1, 0.6]
               }
    
    #смотрим время пересечения эталона и псевдоэталона, а также интервал температур на этот период
    if find_time_intersection(ref, df[f"{gas}op1"], df[f"{gas}t"], t_s, t_f)==True:
        #фиттируем коэффициенты чувствительности
        tmp=pd.concat([
                          df[module_set[gas]].rename(columns={f"{gas}t":"T"}).tz_localize(None),
                          ref.tz_localize(None)
                      ],
                      axis=1).dropna()[t_s:t_f]

        model._fit_finalizer(
                                                tmp.drop(gas, axis=1),
                                                tmp[gas],
                                                *fit_params[gas], p0=gas_P0[gas]
                                             )
        #запишем предсказания псевдоэталона
        prediction=model.predict(df.rename(columns={f"{gas}t":"T"})).rename(gas)
        # display(
        #             draw_plot(prediction.rename(f"Обученный псевдоэталон {gas} из {model.serial_number}"),
                                
        #                         title=f"Значения восстановленных концентраций {gas}",
        #                         yaxis_title=f"C_{gas}, мкг/м3"
        #                      )
        #        )
        print("Параметры модели после калибровки чувствительности:", model, *[f"{a:20s}: {b:10.2e}" for a, b in model.params.items()], sep="\n")
    else:
        return
    return model, prediction

def change_Tsens(m, new_coef):
    """Замена коэффициентов температурной чувствительности, нормированной на температуру 20С, только для моделей NormModelTExp и NormModelTExpH2S.
    {new_coef} - словарь типа:
                        {
                            "comments": "комментарий",
                            'ft0': свободный член,
                            'ft1': линейный член,
                            'ft2': квадратичный член,
                        }
    """
    if m.type=="NormModelTExpH2S" or m.type=="NormModelTExp":
        for coef in ['ft0', 'ft1', 'ft2']:
            m.params[f"{coef}"]=new_coef[f"{coef}"]
        print(f"Выполнена замена температурной чувствительности модели {m.type} {m.gas_name} на зависимость с комментарием \'{new_coef['comments']}\'")
    else:
        print("Неподходящий тип модели для замены температурной чувствительности")
    return m
        

def find_time_intersection(df1, df2, df2_T, crop1, crop2):
    """
    Функция ищет пересечение DataSeries {df1} и {df2} по времени c обрезкой от {crop1} до {crop2}.. Если пересечение есть, то указывает его температурный диапазон из {df2_T}
    """
    tmp=pd.concat([df1.tz_localize(None), df2.tz_localize(None), df2_T.tz_localize(None)], axis=1)[crop1:crop2].dropna()
    if len(tmp)<1:
        print("Нет пересечения данных псевдоэталона с калибруемым устройством, выберите другой набор данных.")
        return False
    else:
        delta=tmp.index[-1]-tmp.index[0]
        print(f"Общий интервал эталона с калибруемым устройством составляет \
{round(delta.days+delta.seconds/86400, 1):>6.1f} дня в диапазоне температур от {df2_T[tmp.index[0]:tmp.index[-1]].quantile(0.02):>5.2f}{176:c}C до {df2_T[tmp.index[0]:tmp.index[-1]].quantile(0.98):>5.2f}{176:c}C.")
        return True
